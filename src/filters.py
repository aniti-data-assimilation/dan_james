"""
This file contains the DAN and function to construct the neural networks
"""
import torch
from torch import nn
from torch.distributions.multivariate_normal import MultivariateNormal as Mvn
import numpy as np 

class DAN(nn.Module):
    """
    A Data Assimilation Network class
    """
    def __init__(self, a_kwargs, b_kwargs, c_kwargs):

        nn.Module.__init__(self)
        self.a = Constructor(**a_kwargs)
        self.b = Constructor(**b_kwargs)
        self.c = Constructor(**c_kwargs)
        #print('network c',self.c)
        
        self.scores = {
            "MSE_b": [],
            "MSE_a": [],
            "RMSE_b": [],
            "RMSE_a": [],
            "LOGPDF_b": [],
            "LOGPDF_a": [],
            "LOSS": []}

    def forward(self, ha, x, y, eval=False):
        """
        forward pass in the DAN
        """
        hb = self.b(ha)  # propagate past mem into prior mem
        pdf_b = self.c(hb)  # translate prior mem into prior pdf        
        ha = self.a(torch.cat((hb, y), dim=1))  # analyze prior mem
        pdf_a = self.c(ha)  # translate post mem into post pdf
        
        # Loss
        logpdf_b = -torch.mean(pdf_b.log_prob(x))
        logpdf_a = -torch.mean(pdf_a.log_prob(x))
        loss = logpdf_b + logpdf_a
        
        if eval:
            with torch.no_grad():
                self.scores["MSE_b"].append(torch.mean(torch.norm(
                    pdf_b.mean - x, dim=1)**2).item())
                self.scores["MSE_a"].append(torch.mean(torch.norm(
                    pdf_a.mean - x, dim=1)**2).item())
                self.scores["RMSE_b"].append(torch.mean(torch.norm(
                    pdf_b.mean - x, dim=1)*x.size(1)**-.5).item())
                self.scores["RMSE_a"].append(torch.mean(torch.norm(
                    pdf_a.mean - x, dim=1)*x.size(1)**-.5).item())
                self.scores["LOGPDF_b"].append(logpdf_b.item())
                self.scores["LOGPDF_a"].append(logpdf_a.item())
                self.scores["LOSS"].append(loss.item())
            
        return loss, ha

    def compute_MSE(self,ha0,xt,yt):
        # MSE over t = 1..T
        T = len(xt)-1
        #print('T',T,ha0.shape,len(xt),len(yt))
        with torch.no_grad():
            #print('pdf_a mean - x',pdf_a.mean - x)
            #print('diff bet. ha and hb',torch.norm(ha-hb,p='fro'))
            ha = ha0
            loss = 0
            loss_a = 0
            loss_b = 0
            mse_a = 0
            mse_b = 0
            for t in range(1, T+1):
                y = yt[t]
                #print('y',t,y.shape)
                #print('ha',t,ha.shape)                

                hb = self.b(ha) 
                pdf_b = self.c(hb)  # translate prior mem into prior pdf      
                #print('hb',t,hb.shape)                
                ha = self.a(torch.cat((hb, y), dim=1))  # analyze prior mem
                pdf_a = self.c(ha)  # translate post mem into post pdf
                
                x = xt[t]
                #print('x',t,x.shape)
                logpdf_b = -torch.mean(pdf_b.log_prob(x))
                logpdf_a = -torch.mean(pdf_a.log_prob(x))
                loss_a += logpdf_a
                loss_b += logpdf_b
                loss += logpdf_b + logpdf_a
                err_a = torch.norm(pdf_a.mean - x, dim=1)**2
                err_b = torch.norm(pdf_b.mean - x, dim=1)**2
                mse_a += torch.mean(err_a)
                mse_b += torch.mean(err_b)
                     
            loss = loss / T
            loss_a = loss_a / T
            loss_b = loss_b / T
            mse_a = mse_a / T
            mse_b = mse_b / T
            
            self.scores["MSE_b"].append(mse_b.item())
            self.scores["MSE_a"].append(mse_a.item())
            self.scores["LOGPDF_b"].append(loss_b.item())
            self.scores["LOGPDF_a"].append(loss_a.item())
            self.scores["LOSS"].append(loss.item())    
            
    def clear_scores(self):
        """ clear the score lists
        """
        for v in self.scores.values():
            v.clear()

class Id(nn.Module):
    """ A simple id function
    """
    def __init__(self):
        nn.Module.__init__(self)

    def forward(self, x):
        """ trivial
        """
        return x


class Subsampler(nn.Module):
    """ A half sub sampling op. H
    """
    def __init__(self):
        nn.Module.__init__(self)

    def forward(self, x):
        """ only 1,3,5,...
        """
        #print(x.shape)
        y = x[:,1::2]
        #print(y.shape)
        return y
    
class Cst(nn.Module):
    """ A constant scale_vec
    """
    def __init__(self, init, dim=None):
        nn.Module.__init__(self)
        if init == "torch":
            k = dim**-.5
            self.c = 2*k*torch.rand(1, dim) - k
        elif isinstance(init, torch.Tensor):
            self.c = init.unsqueeze(0)
        else:
            raise NameError("Cst init unknown")

    def forward(self, x):
        return self.c.expand(x.size(0), self.c.size(0))

    
class EDO(nn.Module):
    """ Integrates an EDO with RK4
    """
    def __init__(self, x_dim, N, dt, init,
                 window=None):
        nn.Module.__init__(self)
        self.x_dim = x_dim
        self.N = N
        self.dt = dt
        if init == "95":
            """ Lorenz95 initialization
            """
            self.window = (-2, -1, 0, 1)
            self.diameter = 4
            self.A = torch.tensor([[[0., 0., 0., 0.],
                                  [-1., 0., 0., 0.],
                                  [0., 0., 0., 0.],
                                  [0., 1., 0., 0.]]])
            self.b = torch.tensor([[0., 0., -1., 0.]])
            self.c = torch.tensor([8.])
        else:
            raise NameError("EDO init not available")

    def edo(self, x):
        """v=
        x-2 x-1 x0 x1
        |   |   |  |
        """
        v = torch.cat(
            [torch.roll(x.unsqueeze(1), -i, 2) for i in self.window], 1)
        v = torch.transpose(v, 1, 2)
        v_flat = v.reshape(-1, self.diameter)
        dx = torch.nn.functional.bilinear(v_flat, v_flat, self.A)\
            + torch.nn.functional.linear(v_flat, self.b, self.c)
        return dx.view(x.size(0), x.size(1))

    def forward(self, x):
        for _ in range(self.N):
            k1 = self.edo(x)
            k2 = self.edo(x + 0.5*self.dt*k1)
            k3 = self.edo(x + 0.5*self.dt*k2)
            k4 = self.edo(x + self.dt*k3)
            x = x + (self.dt/6.0)*(k1 + 2.0*k2 + 2.0*k3 + k4)
        return x


class FullyConnected(nn.Module):
    """ Fully connected NN ending with a linear layer
    """
    def __init__(self, layers, activation_classname):
        nn.Module.__init__(self)
        n = len(layers)
        self.lins = nn.ModuleList(
            [nn.Linear(d0, d1) for
             d0, d1 in zip(layers[:-1], layers[1:])])
        self.acts = nn.ModuleList(
            [eval(activation_classname)() for _ in range(n-2)])

    def forward(self, h):
        for lin, act in zip(self.lins[:-1], self.acts):
            h = act(lin(h))
            #print('layer in FullyConnected',lin,act)
        #print('lin layer directly')
        return self.lins[-1](h)


class FcZero(nn.Module):
    """
    Fully connected neural network with ReZero trick
    """
    def __init__(self, dim, deep, activation_classname):
        """
        layers: the list of the layers dimensions
        """
        #sigma_alpha = 0.1
        nn.Module.__init__(self)
        layers = (deep+1)*[dim]
        self.lins = nn.ModuleList(
            [nn.Linear(d0, d1) for
             d0, d1 in zip(layers[:-1], layers[1:])])
        self.acts = nn.ModuleList(
            [eval(activation_classname)() for _ in range(deep)])
        self.alphas = torch.nn.Parameter(torch.zeros(deep))
        #print('sigma_alpha',sigma_alpha)
        #self.alphas = torch.nn.Parameter(sigma_alpha*torch.randn(deep))

    def forward(self, h):
        for lin, act, alpha in zip(self.lins, self.acts, self.alphas):
            h = h + alpha*act(lin(h))
        return h


class FcZeroLin(nn.Module):
    """
    FcZero network ending with linear layer
    """
    def __init__(self, in_dim, out_dim, deep, activation_classname):
        """
        layers: the list of the layers dimensions
        """
        nn.Module.__init__(self)
        self.fcZero = FcZero(in_dim, deep, activation_classname)
        self.out_dim = out_dim
        assert(out_dim <= in_dim)
        self.lin = FullyConnected([in_dim, out_dim], activation_classname)

    def forward(self, h):
        h = self.fcZero(h)
        #print('h in FcZeroLin',h.shape)
        #h = h[:,0:self.out_dim]
        h = self.lin(h)
        return h


class Gaussian(Mvn):
    """
    Return a pytorch Gaussian pdf from args
    args is either a (loc, scale_tril) or a (x_dim, vec)
    """
    def __init__(self, *args):
        #self.minexp = torch.Tensor([-8.0])
        #self.maxexp = torch.Tensor([8.0])
        if isinstance(args[0], int):
            """args is a (x_dim, vec)
            loc is the first x_dim coeff of vec
            if the rest is one coeff c then
                scale_tril = e^c*I
            else
                scale_tril is filled diagonal by diagonal
                starting by the main one
                (which is exponentiated to ensure strict positivity)
            """
            x_dim, vec = args
            vec_dim = vec.size(-1)
            if vec_dim == x_dim + 1:
                #print('Init Mvn by x_dim+1')
                loc = vec[:, :x_dim]
                scale_tril = torch.eye(x_dim)\
                                  .unsqueeze(0)\
                                  .expand(vec.size(0), -1, -1)
                scale_tril = torch.exp(vec[:, x_dim])\
                                  .view(vec.size(0), 1, 1)*scale_tril
            else:
                #print('Init Mvn by inds')
                inds = self.vec_to_inds(x_dim, vec_dim)
                loc = vec[:, :x_dim]
                diaga = vec[:, x_dim:2*x_dim]
                #diaga = torch.max(self.minexp.expand_as(diaga),diaga)
                #diaga = torch.min(self.maxexp.expand_as(diaga),diaga)
                lbda = torch.cat((torch.exp(diaga),vec[:, 2*x_dim:]), 1)
                #print('min lbda',lbda.min(),'max',lbda.max())
                scale_tril = torch.zeros(vec.size(0), x_dim, x_dim)
                scale_tril[:, inds[0], inds[1]] = lbda
            Mvn.__init__(self, loc=loc, scale_tril=scale_tril)
        
        else:
            """args is a loc, scale_tril
            """
            print('Init Mvn by full arg')
            Mvn.__init__(self, loc=args[0], scale_tril=args[1])

    def vec_to_inds(self, x_dim, vec_dim):
        """Computes the indices of scale_tril coeffs,
        scale_tril is filled main diagonal first

        x_dim: dimension of the random variable
        vec_dim: dimension of the vector containing
                 the coeffs of loc and scale_tril
        """
        ldiag, d, c = x_dim, 0, 0  # diag length, diag index, column index
        inds = [[], []]  # list of line and column indexes
        for i in range(vec_dim - x_dim):  # loop over the non-mean coeff
            inds[0].append(c+d)  # line index
            inds[1].append(c)  # column index
            if c == ldiag-1:  # the current diag end is reached
                ldiag += -1  # the diag length is decremented
                c = 0  # the column index is reinitialized
                d += 1  # the diag index is incremented
            else:  # otherwize, only the column index is incremented
                c += 1
        return inds


class Constructor(nn.Module):
    """Construct functions and conditional Gaussians from strings and kwargs
    - scale_vec_class is not None: return a Gaussian made from a vector,
        this vector is made of the concatenation of loc and scale_vec
    - scale_vec_class is None:
        if gauss_dim is not None: return a Gaussian made from a vector,
        else: return a vector
    """
    def __init__(self, loc_classname, loc_kwargs,
                 gauss_dim=None,
                 scale_vec_classname=None, scale_vec_kwargs=None):
        nn.Module.__init__(self)
        self.gauss_dim = gauss_dim
        self.loc = eval(loc_classname)(**loc_kwargs)
        if scale_vec_classname is not None:
            self.scale_vec =\
                eval(scale_vec_classname)(**scale_vec_kwargs)
        else:
            self.scale_vec = None
            #print('scale_vec is None')

    def forward(self, *args):
        # WARNING copy !
        lc = self.loc(*args)
        if self.gauss_dim is not None:
            if self.scale_vec is not None:
                sc = self.scale_vec(*args)
                #print('lc',lc.shape,'sc',sc.shape)
                #print('compute Gaussian from lc and sc')
                return Gaussian(self.gauss_dim, torch.cat((lc, sc), dim=1))
            else:
                #print('compute Gaussian from lc')
                return Gaussian(self.gauss_dim, lc)
        else:
            return lc
