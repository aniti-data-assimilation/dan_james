from os import mkdir, path
import torch
import sys
import filters
import os
import numpy as np 
from os.path import exists
    
from datetime import datetime
import socket
def get_tkt():
    now = datetime.now() # current date and time
    date_time = now.strftime("%m%d%Y_%H%M%S")
    host_name = socket.gethostname()
    return date_time + '_' + host_name

_v0 = None 
_x0 = None

def get_x0(b_size, x_dim, sigma):
    global _x0
    if _x0 is None:
        _x0 = 3*torch.ones(b_size, x_dim)\
             + sigma * torch.randn(b_size, x_dim)
    x0 = _x0
    return x0

def get_x0_test(b_size, x_dim, sigma):
    _x0_test = 3*torch.ones(b_size, x_dim)\
               + sigma * torch.randn(b_size, x_dim)
    x0 = _x0_test
    return x0

def get_ha0(b_size, h_dim):
    global _v0
    if _v0 is None:
        _v0 = torch.zeros(1,h_dim)
    ha0 = torch.zeros(b_size, h_dim)
    for b in range(b_size):
        ha0[b,:] = _v0
    return ha0

def set_tensor_type(tensor_type,cuda):
    print('use gpu',cuda)
    print('use tensor_type',tensor_type)
    if (tensor_type == "double") and cuda:
        torch.set_default_tensor_type(torch.cuda.DoubleTensor)
    elif (tensor_type == "float") and cuda:
        torch.set_default_tensor_type(torch.cuda.FloatTensor)
    elif (tensor_type == "double") and (not cuda):
        torch.set_default_tensor_type(torch.DoubleTensor)
    elif (tensor_type == "float") and (not cuda):
        torch.set_default_tensor_type(torch.FloatTensor)
    else:
        raise NameError("Unknown tensor_type")
    
def train_online(net, b_size, h_dim, x_dim,
                 T, checkpoint, direxp,
                 prop, obs, sigma0,
                 optimizer_classname, optimizer_kwargs, 
                 scheduler_classname, scheduler_kwargs, burn=0):
    """
    Train functions for the DAN, online and truckated BPTT
    """
    if not path.exists(direxp):
        mkdir(direxp)
        
    # construct optimizer and scheduler
    assert(optimizer_classname != "NONE")
    print(' optimizer_classname', optimizer_classname)
    optimizer = eval(optimizer_classname)(net.parameters(), **optimizer_kwargs)

    assert(scheduler_classname != "NONE")
    print(' scheduler_classname', scheduler_classname)
    scheduler = eval(scheduler_classname)(optimizer, **scheduler_kwargs)
    
    # init
    x0 = get_x0(b_size, x_dim, sigma0)
    ha = get_ha0(b_size, h_dim)

    if burn > 0:
        print('add burning=',burn)
        for t in range(burn):
            x0 = prop.loc(x0)
            
    for t in range(1, T+1):
        # on the fly data generation
        x = prop(x0)\
            .sample(sample_shape=torch.Size([1]))\
            .squeeze(0)
        
        y = obs(x)\
            .sample(sample_shape=torch.Size([1]))\
            .squeeze(0)

        # optimization step
        optimizer.zero_grad()
        loss, ha = net(ha, x, y, eval=True)

        loss.backward()
        optimizer.step()
        scheduler.step()
        
        # Truncated back propagation through time
        ha = ha.detach()
        x0 = x

        # Checkpoint
        if (t % checkpoint == 0) or (t==1) or (t == T):
            print("## Train Cycle " + str(t)+" ##")
            #net.compute_RMSE(ha0,xt,yt)
            save_dict(direxp,scores=net.scores)
            print_scores(net.scores)
            torch.save(net,direxp + '/net_t0_' + str(t) +'.pt')

@torch.no_grad()
def test(net, b_size, h_dim, x_dim,
         T, checkpoint, direxp,
         prop, obs, sigma0, burn=0, skipeval=0):
    x = get_x0_test(b_size, x_dim, sigma0)
    ha = get_ha0(b_size, h_dim)

    if burn > 0:
        print('add burning=',burn)
        for t in range(burn):
            x = prop.loc(x)

    for t in range(1, T+1):
        # on the fly data generation
        x = prop(x)\
            .sample(sample_shape=torch.Size([1]))\
            .squeeze(0)

        y = obs(x)\
            .sample(sample_shape=torch.Size([1]))\
            .squeeze(0)

        # Evaluates the loss
        if t <= skipeval:
            _, ha = net(ha, x, y, eval=False)
        else:
            _, ha = net(ha, x, y, eval=True)

        # Checkpoint
        if (t % checkpoint == 0) or (t == T):
            print("## Test Cycle " + str(t)+" ##")            
            save_dict(direxp,
                      test_scores=net.scores)
            print_scores(net.scores)


def experiment(tensor_type, seed,
               net_classname, net_kwargs,
               sigma0, prop_kwargs, obs_kwargs,
               train_kwargs, test_kwargs,
               optimizer_classname, optimizer_kwargs,
               scheduler_classname, scheduler_kwargs,
               directory, nameexp, burn=0):

    # CPU or GPU tensor
    cuda = torch.cuda.is_available()
    set_tensor_type(tensor_type,cuda)
    
    # Reproducibility
    if seed > 0:
        torch.manual_seed(seed)

    net = eval(net_classname)(**net_kwargs)
    prop = filters.Constructor(**prop_kwargs)
    obs = filters.Constructor(**obs_kwargs)
    b_size = train_kwargs['b_size']
    h_dim = train_kwargs['h_dim']
    x_dim = train_kwargs['x_dim']
    T = train_kwargs['T']
    checkpoint = train_kwargs['checkpoint']
    direxp = directory + nameexp

    if not exists(direxp + '/net.pt'):
        train_online(net, b_size, h_dim, x_dim,
                     T, checkpoint, direxp,
                     prop, obs, sigma0,
                     optimizer_classname, optimizer_kwargs, 
                     scheduler_classname, scheduler_kwargs, burn)
        torch.save(net,direxp + '/net.pt')
    else:
        print('load DAN network')
        net = torch.load(direxp + '/net.pt')

    # Clear scores
    net.clear_scores()

    # Testing
    b_size = test_kwargs['b_size']
    h_dim = test_kwargs['h_dim']
    x_dim = test_kwargs['x_dim']
    T = test_kwargs['T']
    checkpoint = test_kwargs['checkpoint']

    test(net, b_size, h_dim, x_dim,
         T, checkpoint, direxp,
         prop, obs, sigma0, burn)    


def save_dict(prefix, **kwargs):
    """
    saves the arg dict val with name "prefix + key + .pt"
    """
    for key, val in kwargs.items():
        torch.save(val, prefix + '/' + key + ".pt")


def print_scores(scores):
    for key, val in scores.items():
        if len(val) > 0:
            print(key+"= "+str(val[-1]))


def update(k_default, k_update):
    """Update a default dict with another dict
    """
    for key, value in k_update.items():
        if isinstance(value, dict):
            k_default[key] = update(k_default[key], value)
        else:
            k_default[key] = value
    return k_default


def update_and_save(k_default, list_k_update, name_fun):
    """update and save a default dict for each dict in list_k_update,
    generates a name for the exp with name_fun: dict -> string
    returns the exp names on stdout
    """
    out, directory = "", k_default["directory"]
    for k_update in list_k_update:
        nameexp = name_fun(k_update)
        if not os.path.exists(nameexp):
            os.mkdir(nameexp)
        k_default["nameexp"] = nameexp + "/"
        torch.save(update(k_default, k_update), nameexp + "/kwargs.pt")
        out += directory + "," + nameexp

    # return the dir and nameexp
    sys.stdout.write(out)


if __name__ == "__main__":
    """
    the next argument is the experiment name
    - launch the exp
    """
    torch.autograd.set_detect_anomaly(True)
    cuda = torch.cuda.is_available()
    device = torch.device("cuda" if cuda else "cpu")
    experiment(**torch.load(sys.argv[1] + "/kwargs.pt",
                            map_location=torch.device(device)))
