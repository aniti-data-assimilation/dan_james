from numpy import outer, ones, eye, zeros, dot, diag, concatenate, save
from numpy.random import randn, seed
from numpy.linalg import norm, svd, solve
from lorenz95np import M, moy, ano, ens, SR
import sys
import os

# Parameters
Ne = 20
print('m',Ne)

Na = 6*10**5 # T

burn =  int(sys.argv[1]) # 10**3
print('burn',burn)

Nq = 41 # d + 1
Ns = 40 # d
Ndt = 1 # dt = Ndt * 0.05 
b = 1 # sigma backgound?, anyway sigma0 = 1
q = (0.01*Ndt)**0.5 # sigma_prop = sqrt(0.01) = 0.1
r = 1 # sigma_obs
jmax = 20
eps = 10**-3
inflation = 1.1
nrep = 1 # 10

def H(x):
    return x


for rep in range(nrep):
    print("rep= "+str(rep))
    #seed((rep+1)*2)
    # Definitions
    I = eye(Ne+Nq)
    xt = 3*ones(Ns)+randn(Ns)  # truth
    xt = M(xt, burn*Ndt) # burning
    Eb0 = ens(xt, b*randn(Ns, Ne))  # background ensemble
    # Aq1 s.t. Aq1*Aq1.T = q**2*I and Aq1*1=0 with householder matrix
    Aq1 = zeros((Ns, Nq))
    Aq1[:Ns, :Ns] = q*eye(Ns)
    v = ones(Nq)
    v = v/norm(v)
    v[Nq-1] -= 1
    Aq1 = dot(Aq1, eye(Nq)-2*outer(v, v)/norm(v)**2)

    # Assimilation
    i, rmse_a, rmse_b = 0, [], []
    while i < Na:

        # progress
        if i % 1000 == 0:
            sys.stdout.write(str(100*i/Na)+"%\n")

        # background statistics
        xb0, Ab0 = moy(Eb0), ano(Eb0)

        # observation sampling
        xt1 = M(xt, Ndt) + q*randn(Ns)
        y1 = H(xt1) + r*randn(Ns)

#        rmse_b.append(norm(xb0-xt1)*Ns**-0.5)

        # Gauss-Newton
        hessJ, w, j, crit = I, zeros(Ne+Nq), 0, 1
        while j == 0 or (crit >= eps and j < jmax):

            # Initialisation T0
            u, s, v = svd(hessJ[:Ne, :Ne])
            T0 = dot(u, dot(diag(s**-0.5), v))
            invT0 = dot(u, dot(diag(s**0.5), v))

            # Initialisation E0
            x0 = xb0 + dot(Ab0, w[:Ne])
            A0 = dot(Ab0, T0)
            E0 = ens(x0, A0)

            # Propagation
            ME0 = M(E0, Ndt)

            # Eq1 init
            x1 = moy(ME0) + dot(Aq1, w[Ne:])
            Eq1 = ens(x1, Aq1)

            if j == 0:
                rmse_b.append(norm(x1-xt1)*Ns**-0.5)

            # Derivatives
            HMAb0 = dot(ano(H(ME0)), invT0)
            HAq1 = ano(H(Eq1))
            HAb1 = concatenate((HMAb0, HAq1), axis=1)

            # cost function init
            gradJ = w - dot(HAb1.T, y1 - H(x1))/r**2
            hessJ = I + dot(HAb1.T, HAb1)/r**2

            # solves Gauss-Newton system
            dw = -solve(hessJ, gradJ)
            w += dw

            # crit update
            crit = norm(dw)*(Ne+Nq)**-0.5
            j += 1

        # T1 init
        u, s, v = svd(hessJ)
        T1 = dot(u, dot(diag(s**-0.5), v))
        invT1 = dot(u, dot(diag(s**0.5), v))

        # Anomalies update
        MAb0 = dot(ano(ME0), invT0)
        A1 = dot(concatenate((MAb0, Aq1), axis=1), T1)

        # Size reduction and inflation
        A1 = inflation*SR(A1, Ne)

        # E1 init
        E1 = ens(x1, A1)

        # Propagation
        Eb0 = E1
        xt = xt1

        # filtering performance
        rmse_a.append(norm(x1-xt)*Ns**-0.5)

        i += 1

    DIR = "./baseline_results_burning/m" + str(Ne) + "_burn" + str(burn) + '/'
    if not os.path.exists(DIR):
        os.mkdir(DIR)
    save(DIR+"IEnKFQ_test_rmse_b_"+str(rep)+".npy", rmse_b)
    save(DIR+"IEnKFQ_test_rmse_a_"+str(rep)+".npy", rmse_a)
