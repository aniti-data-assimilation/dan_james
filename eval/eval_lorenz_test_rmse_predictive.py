# load DAN at t0=T to eval average RMSE
import torch
import numpy as np
from manage_exp import set_tensor_type, test, save_dict
import filters
import csv
import sys
import os

direxp = sys.argv[1]
m = int(sys.argv[2])

kwargs = torch.load(direxp + '/kwargs.pt')
T = 6*(10**5)
b_size = 1
x_dim = 40
h_dim = m*x_dim
assert(kwargs['net_kwargs']['a_kwargs']['loc_kwargs']["out_dim"] == h_dim)
sigma0 = 1.0
burn = 10**3
checkpoint = T
tensor_type = 'float'

cuda = torch.cuda.is_available()
set_tensor_type(tensor_type,cuda)

if cuda:
    kwargs["prop_kwargs"]["scale_vec_kwargs"]["init"] = kwargs["prop_kwargs"]["scale_vec_kwargs"]["init"].cuda()
    kwargs["obs_kwargs"]["scale_vec_kwargs"]["init"] = kwargs["obs_kwargs"]["scale_vec_kwargs"]["init"].cuda()

prop = filters.Constructor(**kwargs["prop_kwargs"])
obs = filters.Constructor(**kwargs["obs_kwargs"])

# open the file in the write mode

print('t0','testloss')
t0 = T
fa = open(direxp + '/test_rmse_a_T_2T.csv', 'w')
fb = open(direxp + '/test_rmse_b_T_2T.csv', 'w')
writera = csv.writer(fa)
writerb = csv.writer(fb)
for rep in range(1): # 10
    # create the csv writer
    net = torch.load(direxp + '/net_t0_' + str(max(t0,1)) + '.pt')
    net.clear_scores()
    test(net, b_size, h_dim, x_dim,
         2*T, checkpoint, direxp,
         prop, obs, sigma0, burn, skipeval=T)

    # write a row to the csv file
    writera.writerow([rep , np.mean(np.array(net.scores["RMSE_a"])) ])
    fa.flush()
    writerb.writerow([rep , np.mean(np.array(net.scores["RMSE_b"])) ])
    fb.flush()

# close the file
fa.close()
fb.close()
