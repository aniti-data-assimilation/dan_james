# load DAN at various t0
import torch
import numpy as np
from manage_exp import set_tensor_type, test, save_dict
import filters
import csv
import time
import os
import sys

#direxp = 'lorenz_exp6e5_03102022_204214_co2-slurm-ng03' # m=20, 6890084
#m = 20

#direxp = 'lorenz_exp6e5_03182022_170738_co2-slurm-ng07'
#m = 5

direxp = sys.argv[1]
m = int(sys.argv[2])

seed = 1234

kwargs = torch.load(direxp + '/kwargs.pt')
T = 6*(10**5)

b_size = 1024
x_dim = 40
h_dim = m*x_dim
assert(kwargs['net_kwargs']['a_kwargs']['loc_kwargs']["out_dim"] == h_dim)
sigma0 = 1.0
burn = 10**3
checkpoint = T
tensor_type = 'float'

cuda = torch.cuda.is_available()
set_tensor_type(tensor_type,cuda)

if cuda:
    kwargs["prop_kwargs"]["scale_vec_kwargs"]["init"] = kwargs["prop_kwargs"]["scale_vec_kwargs"]["init"].cuda()
    kwargs["obs_kwargs"]["scale_vec_kwargs"]["init"] = kwargs["obs_kwargs"]["scale_vec_kwargs"]["init"].cuda()
    
prop = filters.Constructor(**kwargs["prop_kwargs"])
obs = filters.Constructor(**kwargs["obs_kwargs"])

print('t0','testloss')
# open the file in the write mode

T0 = 0 # if restart set it to another value, e.g. 340000 ,  590000

for rep in range(1):
    if T0 == T:
        f = open(direxp + '/test_loss_T_rep' + str(rep) + '.csv', 'w')
        fa = open(direxp + '/test_lossa_T_rep' + str(rep) + '.csv', 'w')
        fb = open(direxp + '/test_lossb_T_rep' + str(rep) + '.csv', 'w')
    else:
        f = open(direxp + '/test_loss_rep' + str(rep) + '.csv', 'w+')
        fa = open(direxp + '/test_lossa_rep' + str(rep) + '.csv', 'w+')
        fb = open(direxp + '/test_lossb_rep' + str(rep) + '.csv', 'w+')

    # create the csv writer
    writer = csv.writer(f)
    writera = csv.writer(fa)
    writerb = csv.writer(fb)
    for t0 in range(T0,T+1,10000):
        print('t0=',t0)
        t = time.time()
        net = torch.load(direxp + '/net_t0_' + str(max(t0,1)) + '.pt')
        net.clear_scores()
        if seed > 0:
            torch.manual_seed(seed) # use same test data
        test(net, b_size, h_dim, x_dim,
             T, checkpoint, direxp, # T
             prop, obs, sigma0, burn)

        if T0 == 0:
            outfol = direxp + '/loss_t0_' + str(max(t0,1)) + '_rep_' + str(rep)
        else:
            outfol = direxp + '/lossT_t0_' + str(max(t0,1)) + '_rep_' + str(rep)

        if not os.path.exists(outfol):
            os.mkdir(outfol)
        save_dict(outfol,test_scores=net.scores)

        # do stuff
        elapsed = time.time() - t
        print('time is',elapsed, flush=True)
        # write a row to the csv file
        writer.writerow([t0 , np.mean(np.array(net.scores["LOSS"])) ])
        writera.writerow([t0 , np.mean(np.array(net.scores["LOGPDF_a"])) ])
        writerb.writerow([t0 , np.mean(np.array(net.scores["LOGPDF_b"])) ])
        f.flush()
        fa.flush()
        fb.flush()

    # close the file
    f.close()
    fa.close()
    fb.close()
