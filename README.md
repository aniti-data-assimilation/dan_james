# Code to reproduce results of DAN in the paper: Data Assimilation Networks
## draft: https://arxiv.org/abs/2010.09694
### Releasing under an Apache-2.0 license.

### To run the training of DAN, switch to ./src/
with full H

```
python main.py -save lorenz_exp6e5.py -run
```

with partial H

```
python main.py -save lorenz_exp6e5_partialH -run
```

### To evaluate the RMSE/loss of DAN, switch to ./eval/
run the code *.py by specifying the name of the run id, and the ensemble size m (e.g. below with m=20)

```
python *.py lorenz_exp6e5_03102022_204214_co2-slurm-ng03 20
```

### To plot the results of RMSE/loss of DAN, switch to ./plot/
check the *.ipynb files using jupyter notebook

### The baseline code iEnKF-Q is available in ./iENKF-Q/


The code of DAN is recommended to run with a GPU card. It was run with pytorch 1.9.0, cuda 11. Please contact the authors if you have any further questions.

